<?php

/**
 * File for function declaration.
 *
 * @package   WPS_Framework
 * @version   1.0.0
 * @author    Alexander Laznevoy 
 * @copyright Copyright (c) 2017, Alexander Laznevoy
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 *
 */



####################################################
################ SEO Module Example ################
####################################################
new WPS_SEO(
  array(
    "taxonomies"    => array( 'blog_category' ),
    "post_types"    => array( 'blog', 'page'  ),
    "post_archives" => array( 'blog' )
  )
);



####################################################
################ Custom Type Example ###############
####################################################
new WPS_CustomType(
  array(
    /* Create Files */
    'create_archive_file' => false,
    'create_single_file'  => false,

    /* Post Type Register */
    'register_post_type' => array(
      'post_type' => 'blog', // 1) custom-type name
      // labels
      'labels'    => array(
        'name'          => 'Блог',
        'singular_name' => 'Блог', 
        'menu_name'     => 'Блог'
      ),
      // supports_label
      'supports_label' => array(
        'title',
        'comments'
        //'thumbnail', 
        //'editor',
        //'custom-fields',
      ),
      // rewrite
      'rewrite' => array(
        'slug'         => 'blog', // 2) custom-type slug
        'with_front'   => false,
        'hierarchical' => true
      ),
      // general
      'general' => array(
        'taxonomies'        => array('example_tax'), // 3) 
        'menu_icon'         => 'dashicons-welcome-write-blog', // 4) https://developer.wordpress.org/resource/dashicons/
      )
    ),

    /* Create Taxonomy */
    'register_taxonomy' => array(
      // tax
      array (
        'taxonomy_name' => 'blog_category',         // 1) 
        'setting' => array(
          'label'             => 'Рубрики', // 2) 
          'hierarchical'      => true,
          'public'            => true,
          'query_var'         => true,
          'rewrite'           => array( 
            'slug'         => 'blog-category',      // 3)
            'with_front'   => true,
            'hierarchical' => true
          ),
          'show_admin_column' => true, 
          'show_ui'           => true 
        )
      ),
      // tax
    )

  )
);



new WPS_CustomType(
  array(
    /* Create Files */
    'create_archive_file' => false,
    'create_single_file'  => false,

    /* Post Type Register */
    'register_post_type' => array(
      'post_type' => 'reviews', // 1) custom-type name
      // labels
      'labels'    => array(
        'name'          => 'Отзывы',
        'singular_name' => 'Отзывы', 
        'menu_name'     => 'Отзывы'
      ),
      // supports_label
      'supports_label' => array(
        'title',
        'thumbnail', 
        'editor',
        //'custom-fields',
      ),
      // rewrite
      'rewrite' => array(
        'slug'         => 'reviews', // 2) custom-type slug
        'with_front'   => false,
        'hierarchical' => true
      ),
      // general
      'general' => array(
        'query_var'           => false, 
        'publicly_queryable'  => false,
        'exclude_from_search' => true,
        'taxonomies'        => array(''), // 3) 
        'menu_icon'         => 'dashicons-testimonial', // 4) https://developer.wordpress.org/resource/dashicons/
      )
    ),

  )
);


new WPS_CustomType(
  array(
    /* Create Files */
    'create_archive_file' => false,
    'create_single_file'  => false,

    /* Post Type Register */
    'register_post_type' => array(
      'post_type' => 'partners', // 1) custom-type name
      // labels
      'labels'    => array(
        'name'          => 'Отзывы партнеров',
        'singular_name' => 'Отзывы партнеров', 
        'menu_name'     => 'Отзывы партнеров'
      ),
      // supports_label
      'supports_label' => array(
        'title',
        'thumbnail', 
        'editor',
        //'custom-fields',
      ),
      // rewrite
      'rewrite' => array(
        'slug'         => 'reviews', // 2) custom-type slug
        'with_front'   => false,
        'hierarchical' => true
      ),
      // general
      'general' => array(
        'query_var'           => false, 
        'publicly_queryable'  => false,
        'exclude_from_search' => true,
        'taxonomies'        => array(''), // 3) 
        'menu_icon'         => 'dashicons-testimonial', // 4) https://developer.wordpress.org/resource/dashicons/
      )
    ),

  )
);


new WPS_CustomType(
  array(
    /* Create Files */
    'create_archive_file' => false,
    'create_single_file'  => false,

    /* Post Type Register */
    'register_post_type' => array(
      'post_type' => 'partners_blog', // 1) custom-type name
      // labels
      'labels'    => array(
        'name'          => 'Наши партнеры',
        'singular_name' => 'Наши партнеры', 
        'menu_name'     => 'Наши партнеры'
      ),
      // supports_label
      'supports_label' => array(
        'title',
        'thumbnail', 
        //'editor',
        //'custom-fields',
      ),
      // rewrite
      'rewrite' => array(
        'slug'         => 'partners_blog', // 2) custom-type slug
        'with_front'   => false,
        'hierarchical' => true
      ),
      // general
      'general' => array(
        'query_var'           => false, 
        'publicly_queryable'  => false,
        'exclude_from_search' => true,
        'taxonomies'        => array(''), // 3) 
        'menu_icon'         => 'dashicons-groups', // 4) https://developer.wordpress.org/resource/dashicons/
      )
    ),

  )
);



new WPS_MetaBox(
  array(
    'meta_box_name'   => 'Блог',
    'post_types'      => array( 'blog' ),
    'page_templates'  => array( ),
    'meta_box_groups' => array(
      // Group fields
      array(
        'title'    => '',
        'fields'   => array(
          // FIELDS
          array(
            'field_type'   => 'wp_editor',
            'field_name'   => 'wp_editor',
            'title'        => 'Контент записи',
            'options' => array(
              'media_buttons' => 1,
            )
          ),

          // FIELDS
          array(
            'field_type'   => 'wp_editor',
            'field_name'   => 'wp_editor_short',
            'title'        => 'Короткое описание',
            'description'  => 'Для страницы "Блог" ',
          ),

        )
      ),
      // Group fields
    )
  )
);


new WPS_MetaBox(
  array(
    'meta_box_name'   => 'Партнеры',
    'post_types'      => array( 'partners_blog' ),
    'page_templates'  => array( ),
    'meta_box_groups' => array(
      // Group fields
      array(
        'title'    => '',
        'fields'   => array(
          // FIELDS
          array(
            'field_type'   => 'input',
            'field_name'   => 'site_url',
            'title'        => 'Ссылка на сайт партнера',
            'type_input'   => 'url',
            'required'     => true,
          ),

        )
      ),
      // Group fields
    )
  )
);

####################################################
################ PostColumns Example ###############
####################################################
new WPS_PostColumns(
  array(
    'post_type' => 'blog',
    'fields'    => array(
      array(
        'field_type'   => 'views',
        'columns_name' => 'Просмотры'
      ),
    )
  )
);