<?php
/**
 * WPS Filters.
 *
 * @package    WPS_Framework
 * @subpackage Functions
 * @author     Alexander Laznevoy 
 * @copyright  Copyright (c) 2017, Alexander Laznevoy
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

// Add favicon tags to page
add_action( 'wp_head', 'wps__favicon_tags' );
