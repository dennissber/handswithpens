$(document).ready(function(){

  // text
  $("body").on( "submit", ".form_send_text", function(){
    var form        = $(this);
    var btn_submit  = form.find('[type=submit]');
 
    $.ajax({
      url : theme_ajax.url, 
      type: 'POST', 
      data: form.serialize(),
      dataType:'json',
      processData: false,
      beforeSend: function(xhr){
        form.addClass('sending');
        btn_submit.addClass('sending');
      },
      success: function(data) {
        form.trigger('reset');
        if( data.g_error ){ 
          console.log(data.g_error); 
        } else if ( data.location ) { 
          // if redirect
          window.location.replace(data.location+"/");
        } else {
          if ( btn_submit ) {
            btn_submit.val(data.success);
          }
          // clear sending
          form.removeClass('sending');
          btn_submit.removeClass('sending');
          // mark success
          form.addClass('success');
          btn_submit.addClass('success');
          // clear succes
          setTimeout(function(){
            form.removeClass('success');
            btn_submit.removeClass('success');
          }, 2500);
        }
      },
      error: function(data){
        console.log("Mail ajax error");
      }
    });
    return false;
  });


  // text & file
  $("body").on( "submit", ".form_send_file", function(){
    var form        = $(this);
    var btn_submit  = form.find('[type=submit]');

    var data = new FormData($(this)[0]);

    $.ajax({
      url : theme_ajax.url, 
      type: 'POST', 
      data: data,
      dataType:'json',
      processData: false,
      contentType: false,
      beforeSend: function(xhr){ 
        form.addClass('sending');
        btn_submit.addClass('sending');
      },
      success: function(data) {
        form.trigger('reset');
        // if redirect
        if( data.g_error ){ 
          console.log(data.g_error); 
        } else if ( data.location ) {
          // if redirect
          window.location.replace(data.location+"/");
        } else {
          if ( btn_submit ) {
            btn_submit.val(data.success);
          }
          // clear sending
          form.removeClass('sending');
          btn_submit.removeClass('sending');
          // mark success
          form.addClass('success');
          btn_submit.addClass('success');
          // clear succes
          setTimeout(function(){
            form.removeClass('success');
            btn_submit.removeClass('success');
          }, 2500);
        }
      },
      error: function(data){
        console.log("Mail ajax error");
      }
    });
    return false;
  });
  
});