<?php
/**
 * MAIL Module.
 *
 * @package   WPS_Framework
 * @version   1.0.0
 * @author    Alexander Laznevoy 
 * @copyright Copyright (c) 2017, Alexander Laznevoy
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
*/


new WPS_OptionPage(
  array(
    /* submenu_setting */
    'submenu_setting' => array(
      'submenupos' => "wps_framework",
      //'submenupos' => "edit.php?post_type=custom_post2",
      'page_title' => 'MAIL Module',
      'menu_title' => 'MAIL Module',
      'capability' => 'administrator',
      'menu_slug'  => 'wps_mail_module_settings',
    ),
    /* submenu_setting */
    'fields'    => array(

      array(
        'field_type'  => 'input',
        'field_name'  => 'theme_email',
        'title'       => 'E-mail для писем',
        'description' => "Можно ввести несколько почтовых адресов через запятую"
      ),

      array(
			  'field_type'   => 'checkbox',
			  'field_name'   => 'mail_in_admin_panel',
			  'title'        => 'Вывести почту в админ-панель?',
			),

    )
  )
);


/* 
<!-- hidden input -->
<input type="hidden" name="form_subject"  value="">
<input type="hidden" name="form_title"    value="">
<input type="hidden" name="form_redirect" value="">
<input type="hidden" name="btn_response"  value="">
<input type="hidden" name="action"        value="form_send_text">
<!-- hidden input -->
*/



class WPS_Mail {

  // main options module
  private $options; 
  
  function __construct() {
    $this->options = get_option('wps_mail_module_settings');

    // name form
    add_action('wp_ajax_nopriv_form_send_text', array( $this, 'form_send_text'));
    add_action('wp_ajax_form_send_text', array( $this, 'form_send_text'));

    // name form
    add_action('wp_ajax_nopriv_form_send_file', array( $this, 'form_send_file'));
    add_action('wp_ajax_form_send_file', array( $this, 'form_send_file'));

    // init_script
    add_action( 'wp_enqueue_scripts', array($this,'init_script') );

    // выводить интерфейс почты в админку?
    if ( $this->options['mail_in_admin_panel'] ){
    	$this->wps_init_mail_type();
    }

  }
  
  // init_script
  public function init_script(){
    wp_enqueue_script  ( 'wps_mail_action', trailingslashit( WPS_MODULES_URI ) . 'wps_mail/wps_mail_action.js', array('jquery'), WPS_VERSION, true );
    wp_localize_script ( 'wps_mail_action', 'theme_ajax',
      array(
        'url' => admin_url('admin-ajax.php')
      )
    );
  }


  ## form text
  public function form_send_text(){

    // email
    $to = $this->options['theme_email'];
    if ( !$to ){
      $result['g_error'] = $to."Не указана почта!";
      exit( json_encode($result) );
    }

    /* base */
    $sender        = 'wordpress@' . wps__get_sitename();
    $project_name  = wps__get_sitename()." ";
    $subject       = $_POST["form_subject"] ? htmlspecialchars( $_POST["form_subject"] ) : "No subject";
    // other
    $form_redirect = htmlspecialchars( $_POST["form_redirect"] );
    $form_title    = $_POST["form_title"]   ? htmlspecialchars( $_POST["form_title"] )   : $subject;
    $btn_response  = $_POST["btn_response"] ? htmlspecialchars( $_POST["btn_response"] ) : "Отправлено!";


    /* msg */
    $message = $this->render_message( $_POST, $form_title );

    // save data 
    $this->wps_save_mail( "wps_mail", $subject, $form_title, $message );


    /* header */
    $headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
    $headers .= 'From: ' . $project_name . '<'. $sender . '>';

    /* send mail */
    if( wp_mail( $to, $subject, $message, $headers ) ) {
      $result['success'] = $btn_response;
      // form_redirect
      if ( $form_redirect != '' ) {
        $result['location'] = $form_redirect;
      }
      exit( json_encode($result) );
    } else {
      $result['g_error'] = "Ошибка!";
      exit( json_encode($result) );
    }
  }



  ## form text & file
  public function form_send_file(){

    // email
    $to = $this->options['theme_email'];
    if ( !$to ){
      $result['g_error'] = $to."Не указана почта!";
      exit( json_encode($result) );
    }

    /* base */
    $sender       = 'wordpress@' . wps__get_sitename();
    $project_name = wps__get_sitename()." ";

    /* other */
    $subject       = $_POST["form_subject"] ? htmlspecialchars( $_POST["form_subject"] ) : "No subject";
    $form_redirect = htmlspecialchars( $_POST["form_redirect"] );
    $form_title    = $_POST["form_title"]   ? htmlspecialchars( $_POST["form_title"] )   : $subject;
    $btn_response  = $_POST["btn_response"] ? htmlspecialchars( $_POST["btn_response"] ) : "Отправлено!";

    if (!empty($_FILES['file']['tmp_name'])) {
        $file_tmp  = $_FILES["file"]['tmp_name'];
        $file_name = $_FILES["file"]['name'];
    }

    /* msg */
    $message = $this->render_message( $_POST, $form_title );

    $boundary = "--".md5(uniqid(time())); 
    // генерируем разделитель

    /* header */
    $mailheaders  = "MIME-Version: 1.0;\r\n"; 
    $mailheaders .="Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n"; 
    // разделитель указывается в заголовке в параметре boundary 
    $mailheaders .= "From: $project_name <$sender>\r\n"; 

    /* multipart */
    $multipart  = "--$boundary\r\n"; 
    $multipart .= "Content-Type: text/html; charset=utf-8\r\n"; 
    $multipart .= "\r\n";
    $multipart .= $message;


    if( $file_name ) {
      $fp = fopen($file_tmp, "rb");
      $file = fread($fp, filesize($file_tmp));
      fclose($fp);

      $message_part  = "\r\n--$boundary\r\n"; 
      $message_part .= "Content-Type: application/octet-stream; name=\"$file_name\"\r\n";  
      $message_part .= "Content-Transfer-Encoding: base64\r\n"; 
      $message_part .= "Content-Disposition: attachment; filename=\"$file_name\"\r\n"; 
      $message_part .= "\r\n";
      $message_part .= chunk_split(base64_encode($file));
      $message_part .= "\r\n--$boundary--\r\n";
      // второй частью прикрепляем файл, можно прикрепить два и более файла
      $multipart .= $message_part;
    }


    // save data 
    $this->wps_save_mail( "wps_mail", $subject, $form_title, $message );

    /* send mail */
    if( wp_mail( $to, $subject, $multipart, $mailheaders ) ) {
      $result['success'] = $btn_response;
      // form_redirect
      if ( $form_redirect != '' ) {
        $result['location'] = $form_redirect;
      }
      exit( json_encode($result) );
    } else {
      $result['g_error'] = "Ошибка!";
      exit( json_encode($result) );
    }

  }

  /* render_message */
  private function render_message( $post, $form_title ){

  	if ( !is_array( $post ) ) exit();

  	/* remove */
  	unset( 
  		$post['action'], 
  		$post['form_subject'], 
  		$post['form_redirect'], 
  		$post['btn_response'], 
  		$post['form_title']
  	);

  	/* msg */
    $message  = '<html><body>';
    $message .= '<table cellspacing="0" align="center" border="1" bgcolor="#F8F8F8" cellpadding="0" style="width:100%; max-width:600px;" >';
    $message .= '<tr><td colspan="2" style="padding: 5px 10px; text-align:center;">'.$form_title.'</td></tr>';
    foreach ($post as $key => $value) {
      if ( $value != "" ) {
       $message .= '
        <tr>
          <td width="30%" style="padding: 5px 10px;">'.$key.':</td>
          <td style="padding: 4px 8px;">'.$value.'</td>
        </tr>';
      }
    }
    $message .= '</table>'; 
    $message .= '</body></html>';

    return $message;
  }


  private function wps_init_mail_type(){

		################ Mail Type ###############
		new WPS_CustomType(
		  array(
		    /* Create Files */
		    'create_archive_file' => false,
		    'create_single_file'  => false,

		    /* Post Type Register */
		    'register_post_type' => array(
		      'post_type' => 'wps_mail', // 1) custom-type name
		      // labels
		      'labels'    => array(
		        'name'          => 'Mail',
		        'singular_name' => 'Mail', 
		        'menu_name'     => 'Mail'
		      ),
		      // supports_label
		      'supports_label' => array(
		        'title',
		      ),
		      // rewrite
		      'rewrite' => array(
		        'slug'         => 'wps_mail', // 2) custom-type slug
		        'with_front'   => false,
		        'hierarchical' => true
		      ),
		      // general
		      'general' => array(
		        /* if need remove in query */
		        'query_var'         => false, 
		        'publicly_queryable'  => false,
		        'exclude_from_search' => true,
		        'taxonomies'        => array('wps_mail_tax'), // 3) 
		        'menu_icon'         => 'dashicons-email-alt', 
		      )
		    ),

		    ################ Mail Tax ###############
		    'register_taxonomy' => array(
		      // tax
		      array (
		        'taxonomy_name' => 'wps_mail_tax',         // 1) 
		        'setting' => array(
		          'label'             => 'Категория', // 2) 
		          'hierarchical'      => true,
		          'publicly_queryable'  => false,
		          'public'            => false,
		          'query_var'         => false,
		          'show_admin_column' => true, 
		          'show_ui'           => true 
		        )
		      ),
		      // tax
		    )

		  )
		);

		################ Mail Meta Fields ###############
		new WPS_MetaBox(
		  array(
		    'meta_box_name'   => 'Почта',                   
		    'post_types'      => array( 'wps_mail' ),   
		    'page_templates'  => array(  ),
		    'meta_box_groups' => array(
		      // GROUP FIELD
		      array(
		        'title'    => '',
		        'fields'   => array(

		        	array(
							  'field_type'   => 'input',
							  'field_name'   => 'counter',
							  'title'        => '№ письма',
							  'type_input'   => 'number',
							),

		          array(
		            'field_type'  => 'wp_editor',
		            'field_name'  => 'post_data',
		            'title'       => 'Содержание письма',
		            'description' => '',
		          ),

		          array(
							  'field_type'   => 'checkbox',
							  'field_name'   => 'viewed',
							  'title'        => 'Просмотрено',
							),

		        )
		      ),
		      // GROUP FIELD
		    )
		  )
		);

		################ Mail Post Columns ###############
		new WPS_PostColumns(
		  array(
		    'post_type' => 'wps_mail',
		    'fields'    => array(
		      array(
		        'field_type'   => 'text',
		        'field_name'   => 'counter',
		        'columns_name' => '№ письма'
		      ),
		      array(
		        'field_type'   => 'checkbox',
		        'field_name'   => 'viewed',
		        'columns_name' => 'Просмотрено'
		      ),
		    )
		  )
		);

  }

  public static function wps_mail( $to, $mail_data ){
  	// ???
  }

  /* wps_save_mail */
  public static function wps_save_mail( $post_type = "wps_mail", $subject, $form_title, $mail_data ){

  	/* mail number */
    $cur_count = get_option( "wps_mail_counter__{$subject}" );
    $new_count = !$cur_count ? (int) 1 : ++$cur_count;
    update_option(  "wps_mail_counter__{$subject}", $new_count );

  	/* set meil to admin */
    $order_detail = array(
      'post_title'   => $subject,
      'post_type'    => $post_type,
      'post_status'  => 'publish',
      'meta_input'   => array(
        'title'     => $form_title,
        'counter'   => $new_count,
        'post_data' => $mail_data, 
      )
    );
    $post_id = wp_insert_post( $order_detail );

	  wp_set_object_terms( $post_id, $subject, 'wps_mail_tax', false ); // false - перезаписать старые связи

  }

}

new WPS_Mail();