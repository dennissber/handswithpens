<?php
/**
 * The admin UI elements select functionality.
 *
 * @package   WPS_Framework
 * @version   1.0.0
 * @author    Alexander Laznevoy 
 * @copyright Copyright (c) 2017, Alexander Laznevoy
 * @license   http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* Example args
array(
  'field_type'   => 'select',
  'field_name'   => 'select',
  'title'        => 'select title',
  'description'  => '',
  'class'        => '',
  'options'      => array(
    'key'  => 'val',
    'key1' => 'val1',
  )
),
*/

// If this file is called directly, abort.
if ( !defined( 'WPINC' ) ) {
  die;
}

class UI_Select {

  // general settings
  private $settings = array();

  // defaults settings
  private $defaults_settings = array(
    'field_name'   => '',              // unique id (without spaces)
    'value'        => array(),         // value
    'options'      => array(),
    'class'        => 'wps_ui_select', // class
  );

  function __construct( $args = array() ) {
    $this->settings = wp_parse_args( $args, $this->defaults_settings );
  }

  public function render() {
    // get setting
    $setting  = $this->settings;
    // other
    $array_path   = $setting['array_path'];
    $value        = $setting['value'];
    $options      = $setting['options'];
    $class        = $setting['class'];

    $html = '';
		$html .= '<select class="'.$class.'" name="'.$array_path.'" />';
		if ( $options ){
			$html .= '<option value="">----</option>';
			foreach ($options as $key => $name) {
				$selected = $value == $key ? 'selected' : '';
				$html .= '<option value="'.$key.'" '.$selected.' >'.$name.'</option>';
			}
		}
		$html .= '</select>';

    return $html;
  }

}
