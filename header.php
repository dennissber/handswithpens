<!DOCTYPE html>
<html <?php language_attributes(); ?> >
  <head>
        
    <meta name="theme-color" content="#2c70e2">
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

  <!-- header -->
  <header>
    <div class="wrapper">
      <div class="header__logo_wrap">
        <a href="" class="header__logo">
          <img src="<?= REL_ASSETS_URI; ?>img/logo_head.png" alt="">
        </a>
      </div>
      <div class="header__right">
        <div class="header__nav_btn" id="header__nav_btn">
          <span></span>
          <span></span>
          <span></span>
        </div>
        <div class="header__nav_wrap" id="header__nav_wrap">
          <?php 
            wp_nav_menu( array(
              'theme_location'  => 'main_menu',
              'container'       => 'nav',
              'container_class' => 'header__nav'
            ));
          ?>
          <div class="header__action_mobile___wrap">
            <a class="header__action__phone" href="tel:+380931336633">
              +38 (093) 133 66 33
            </a>
            <span class="header__action__callback" data-modal="callback">Обратный звонок</span>
          </div>
        </div>
        <div class="header__action_wrap">
          <a class="header__action__phone" href="tel:+380931336633">
            +38 (093) 133 66 33
          </a>
          <span class="header__action__callback" data-modal="callback">Обратный звонок</span>
        </div>
      </div>
      
    </div>
  </header>
  <!-- end header -->

  <!-- main_wrapper -->
  <div class="main_wrapper" id="main_wrapper">