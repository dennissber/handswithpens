<?php get_header(); ?>

<!-- top_screen -->
  <section class="top_screen">
    <div class="wrapper">
      <div class="section_title">
        <h1><?php the_title(); ?></h1>
      </div>
      <span class="decor_text_underline"></span>
    </div>
  </section>
  <div class="decor_line decor_line_top">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
    </svg>
  </div>
  <!-- end top_screen -->

   <section class="simple_page">
    <div class="wrapper">
    	<?php while ( have_posts() ) : the_post(); ?>
    		<?php the_content(); ?>
    	<?php endwhile; ?>
    </div>
  </section>




<?php get_footer(); ?>