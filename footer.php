  
  <!-- footer__socials -->
  <section class="footer__socials">
    <div class="wrapper">
      <a href="https://www.facebook.com/handswithpens.com.ua/" class="footer__socials_item footer__soc__fb"><i class="fa fa-facebook"></i></a>
    </div>
  </section>
  <!-- end footer__socials -->
  

  <div class="decor_line decor_line_bottom">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 100 L100 0 L100 100" stroke-width="0"></path>
    </svg>
  </div>


  <!-- footer -->
  <footer>
    <div class="wrapper">
      <div class="footer__phone">
        <a href="tel:+38(093)1336633">+38 (093) 133 66 33</a>
      </div>
      <div class="footer__right">
        Hands with Pens Украина. All right reserved. 2015-2017
      </div>
    </div>
  </footer>
  <!-- end footer -->
  
  </div>
  <!-- end main wrapper -->
  
  <span class="go_top_btn" id="goTop">
    <i class="fa fa-angle-up"></i>
  </span>
  
  
  <div class="modalOverlay"></div>
  
  <div class="modalWindowWrap callback">
    <div class="modalTable">
      <div class="modalCell">
        <div class="modalWindow">
          <div class="modalWindowClose"></div>
          <p class="form_title">Перезвоните мне</p>
          <form class="form_send_text">
             <div class="form_row">
              <span class="form_row_descrip">Ваше имя</span>
              <input type="text" name="Имя" class="input_text" required autocomplete="off">
             </div>
             <div class="form_row">
              <span class="form_row_descrip">Ваш телефон</span>
              <input type="tel" name="Телефон" class="input_text user_phone_mask" required>
             </div>

            <!-- hidden input -->
            <input type="hidden" name="form_subject"  value="Обратный звонок">
            <input type="hidden" name="form_title"    value="Перезвоните мне">
            <input type="hidden" name="form_redirect" value="">
            <input type="hidden" name="btn_response"  value="Отправлено">
            <input type="hidden" name="action"        value="form_send_text">
            <!-- hidden input -->

             <div class="form_row">
              <input type="submit" class="input_submit" value="Отправить">
             </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- script -->
  <?php wp_footer(); ?>
  <!-- end script -->
  
  </body>
</html>