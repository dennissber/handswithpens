<div class="blog__sidebar">
           
  <div class="blog__sidebar__item">
    <span class="title">Рубрики</span>
    <ul>
      <?php 
      $cur_cat_obj  = get_queried_object();
      $cur_term_id  = $cur_cat_obj->term_id;

      $args = array(
        'taxonomy' => 'blog_category',
        'hide_empty' => true,
      );
      $terms = get_terms( $args );

      foreach ($terms as $key => $value) {
        $term_id = $value->term_id;
        $name    = $value->name;
        $slug    = $value->slug;
        $term_link = get_term_link($term_id);
      ?>
        <li class="<?php if ( $cur_term_id == $term_id ) echo 'active' ?>" >
          <a href="<?= $term_link; ?>" ><?= $name; ?></a>
        </li>
      <?php
      }
      ?>
    </ul>
  </div>
  
  <div class="blog__sidebar__item">
    <span class="title">Последние записи в блоге</span>

    <ul>

      <?php 
        $args_news = array( "post_type" => "blog", "numberposts" => 3, );
        $news_posts = get_posts( $args_news );
      ?>
      <?php foreach($news_posts as $post) { setup_postdata($post); ?>
        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
      <?php } ?>
      <?php wp_reset_postdata(); ?>


    </ul>
    
  </div>
  
  <div class="blog__sidebar__item">
    <span class="title">О Нас</span>
    <p>Hands with Pens - агентство копирайтинга, которое стоит на страже грамотности, полезности и информативности статей на ваших сайтах. Мы постоянно обновляем свои знания и готовы делиться этими знаниями с вами!</p>
  </div>
  
</div>