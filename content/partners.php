<div class="our_partners__wrap">
  <div class="our_partners__slider">

   <?php
      global $post;
      $args = array( 'post_type' => 'partners', 'order' => 'ASC', 'posts_per_page' => -1 );
      $name = get_posts( $args );
      foreach( $name as $post ){ setup_postdata($post);
    ?>

    <div class="our_partners__item">
      <div class="our_partners__item__img">
        <?php the_post_thumbnail(); ?>
      </div>
      <div class="our_partners__item__text">
        <?php the_content(); ?>
      </div>
    </div>
      
    <?php
      }
      wp_reset_postdata();
    ?>
   
  </div>
  
  <span class="slider_prev"></span>
  <span class="slider_next"></span>
</div>