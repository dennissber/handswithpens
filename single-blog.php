<?php get_header(); ?>



  <!-- top_screen -->
  <section class="top_screen">
    <div class="wrapper">
      <div class="section_title">
        <h1>Блог</h1>
      </div>
      <span class="decor_text_underline"></span>
    </div>
  </section>
  <!-- end top_screen -->
  
   <div class="decor_line decor_line_top">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
    </svg>
  </div>
  
  
  <!-- blog_single  -->
  <section class="blog_single">
    <div class="wrapper">
      
      <div class="grid_wrap">
       
        <!-- blog_archive__items -->
        <div class="grid_coll_70">
          
          <div class="blog_archive__item">
            <div class="blog_archive__item__title_wrap__post_icon">
              <i class="fa fa-file-text"></i>
            </div>
            <div class="blog_archive__item__title_wrap">
              <h2 class="post_title">
                <span><?php the_title(); ?></span>
              </h2>
              <small class="post_date"><?php the_time('j F Y') ?></small>
            </div>
            <div class="blog_archive__item__text_wrap">
              <?php echo get_post_meta( $post->ID, 'wp_editor', true ); ?>
            </div>
            
            <div class="blog_archive__item__social_wrap">
              <span class="title">Поделитесь с друзьями!</span>
              <ul class="social_share">
                <li>
                  <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
                    <i class="fa fa-facebook"></i>
                  </a>
                  <span><p>Поделиться в Facebook</p></span>
                </li>
                <li>
                  <a href="https://vk.com/share.php?url=<?php the_permalink(); ?>" target="_blank">
                    <i class="fa fa-vk"></i>
                  </a>
                  <span><p>Поделиться в VK</p></span>
                </li>
              </ul>
            </div>

          </div>
         
           
          <div class="blog_single__comment_wrap">
            <?php comments_template(); ?>
          </div>

        </div>
        <!-- end blog_archive__items -->
        
        <!-- blog__sidebar -->
        <div class="grid_coll_30">
          <?php get_template_part( 'content/blog_sidebar' ); ?>
        </div>
        <!-- end blog__sidebar -->
        
      </div>
      
    </div>
  </section>
  <!-- end blog_single -->

<?php get_footer(); ?>