<?php

// Load the WPS Framework.
require_once( trailingslashit( get_template_directory() ) . 'wps_framework/wps-framework.php' );
new WPS_Framework();

// Sets up theme defaults and registers support for various WordPress features.
add_action( 'after_setup_theme', 'wps__theme_setup' );
function wps__theme_setup() {
  // Load the congif files.
  require_once( trailingslashit( PARENT_DIR ) . 'wps_config/wps_config.php' );
  //require_once( trailingslashit( PARENT_DIR ) . 'wps_config/examlpe.php' );
}


#### Path
define('REL_ASSETS_URI', 'wp-content/themes/'.get_template().'/assets/'); // for IMG in frontend
define('ABS_ASSETS_URI', get_template_directory_uri().'/assets');         // for JS and CSS

#### Подключение скриптов и стилей https://truemisha.ru/blog/wordpress/wp_enqueue_script.html
## Условные теги http://wp-kama.ru/id_89/uslovnyie-tegi-v-wordpress-i-vse-chto-s-nimi-svyazano.html
add_action( 'wp_enqueue_scripts', 'set_scripts_and_styles' );
function set_scripts_and_styles() {
  if( !is_admin() ){

    ## jQuery
    wp_deregister_script( 'jquery' );
    //wp_register_script  ( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js', false, null, true );
    wp_register_script( 'jquery', ABS_ASSETS_URI.'/js/jquery-2.1.4.min.js', false, null, true );

    ## register script and styles
    wp_register_style ( 'main_style', ABS_ASSETS_URI.'/css/style.css', array(), '1.0.3', null );
    wp_register_style ( 'font_awesome', ABS_ASSETS_URI.'/css/font-awesome.min.css', array(), null, null );

    wp_register_script( 'common-js', ABS_ASSETS_URI.'/js/common.js', array('jquery'), '1.0.3', true );
    wp_register_script( 'maskedinput', ABS_ASSETS_URI.'/libs/maskedinput/jquery.maskedinput.min.js', array('jquery'), null, true );
    wp_register_script( 'typed', ABS_ASSETS_URI.'/libs/typed/lib/typed.min.js', array('jquery'), null, true );

    wp_register_style ( 'slick_slider', ABS_ASSETS_URI.'/libs/slick_slider/slick/slick.css', array(), null, null );
    wp_register_script( 'slick_slider', ABS_ASSETS_URI.'/libs/slick_slider/slick/slick.min.js', array('jquery'), null, true );


    //wp_register_style ( 'name', ABS_ASSETS_URI.'/css/', array(), null, null );
    //wp_register_script( 'name', ABS_ASSETS_URI.'/js/', array('jquery'), null, true );

    ## init
    //wp_enqueue_style ( 'name' );
    //wp_enqueue_script( 'name' );

    wp_enqueue_style ( 'main_style' );

    wp_enqueue_script( 'jquery' );

    wp_enqueue_script( 'maskedinput' );
    wp_enqueue_script( 'typed' );

    wp_enqueue_style ( 'slick_slider' );
    wp_enqueue_script( 'slick_slider' );
    
    wp_enqueue_script( 'common-js');
    
  }   
}
## Часть стилей в footer
add_action( 'get_footer', 'my_style_method' );
function my_style_method() {
  wp_enqueue_style ( 'font_awesome' );
  //wp_enqueue_style( 'awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', array(), null, null );
};



add_theme_support('menus');


#### Register menu
add_action('after_setup_theme', function() {
  register_nav_menus( array(
    'main_menu' => 'Основное меню'
  ) );
});



## remove_comment_fields
function remove_comment_fields($fields) {
  unset($fields['url']);
  return $fields;
}
add_filter('comment_form_default_fields','remove_comment_fields');



// Сортировка колонки "Просмотры"
add_filter('manage_edit-blog_sortable_columns', 'add_views_sortable_column');
function add_views_sortable_column($sortable_columns){
  $sortable_columns['columns_title_0'] = array('views', 'desc'); // desc - по умолчанию
  return $sortable_columns;
}

add_filter('pre_get_posts', 'add_column_views_request');
function add_column_views_request( $object ){
  if( $object->get('orderby') != 'views' )
    return;

  $object->set('meta_key', 'wps_post_views_count');
  $object->set('orderby', 'meta_value_num');
}