<?php 
/*
  Template Name: Отзывы веб студий
  */
  get_header(); ?>

  <!-- top_screen -->
  <section class="top_screen">
    <div class="wrapper">
      <div class="section_title">
        <h1><?php the_title(); ?></h1>
      </div>
      <span class="decor_text_underline"></span>
    </div>
  </section>
  <!-- end top_screen -->

   
  <!-- our_partners -->
  <section class="our_partners">
    <div class="wrapper">
    
      <?php get_template_part( 'content/partners' ); ?>
      
      <a href="rewievs_web_studio" class="btn_style_2">все отзывы</a>

    </div>
  </section>
  <!-- end our_partners -->

  <div class="decor_line decor_line_top">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
    </svg>
  </div>


<?php get_footer(); ?>