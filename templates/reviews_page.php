<?php 
/*
  Template Name: Отзывы
  */
  get_header(); ?>

<!-- top_screen -->
  <section class="top_screen">
    <div class="wrapper">
      <div class="section_title">
        <h1><?php the_title(); ?></h1>
      </div>
      <span class="decor_text_underline"></span>
    </div>
  </section>
  <div class="decor_line decor_line_top">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
    </svg>
  </div>
  <!-- end top_screen -->

  <!-- reviews -->
  <section class="reviews">
    <div class="wrapper">
      
      <div class="reviews__wrap">
        <div class="reviews__slider">

          <?php
            global $post;
            $args = array( 'post_type' => 'reviews', 'order' => 'ASC', 'posts_per_page' => -1 );
            $name = get_posts( $args );
            foreach( $name as $post ){ setup_postdata($post);
          ?>

          <div class="reviews__item">
            <div class="reviews__item__img">
              <?php the_post_thumbnail(); ?>
            </div>
            <div class="reviews__item__text">
              <?php the_content(); ?>
            </div>
          </div>
            
          <?php
            }
            wp_reset_postdata();
          ?>
         
        </div>
        
        <span class="slider_prev"></span>
        <span class="slider_next"></span>
      </div>

    </div>
  </section>
  <!-- end reviews -->


<?php get_footer(); ?>