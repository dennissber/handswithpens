<?php
  /*
  Template Name: Партнеры
  */
  get_header();
?>



  <!-- top_screen -->
  <section class="top_screen">
    <div class="wrapper">
      <div class="section_title">
        <h1>Работаете в WEB-студии или <br>SEO-специалистом?</h1>
      </div>
      <span class="decor_text_underline"></span>
    </div>
  </section>
  <!-- end top_screen -->
  
   <div class="decor_line decor_line_top">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
    </svg>
  </div>
  
  
  <!-- partners_page  -->
  <section class="partners_page partners_page_b1">
    <div class="wrapper">
      
      <div class="section_title">
        <h2>Давайте работать вместе, чтобы делать для ваших клиентов еще больше заказов!</h2>
      </div>
      
      <br>
      <br>
      
      <p>Мы в копирайтинге <strong> более 3-х лет.</strong> И сейчас наша <strong>работа налажена для сотрудничества с вами:</strong></p>
      <p>1. Для вас работают <strong> 3 Тим-лидера и 20 копирайтеров</strong>, которые на связи 6 дней в неделю. </p>
      <p>2. При текущей загрузке<strong> готовы писать </strong> более 500 000 символов, а если будет нужно больше - увеличим команду.</p>
      <p>3. Мы на себе <strong>закрываем полный цикл работы с копирайтерами</strong>: сами занимаемся рекрутингом, отбираем лучших, учим и стажируем, а только потом даем им коммерческие проекты. </p>
      <p>4. Работая с нами, вы <strong>сократите расходы </strong>на рабочие места, компьютеры, налоги и зарплаты. Вам не надо будет тратить время на подбор и обучение копирайтеров!</p>
      <p>5. Мы уже работаем как <strong>с крупными WEB-студиями Украины,</strong> так и <strong> с небольшими компаниями и агентствами,</strong> которые регионально удовлетворяют потребности местного бизнеса.</p>
      

    </div>
  </section>
  <!-- end partners_page -->
  
  <div class="decor_line decor_line_bottom">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 100 L100 0 L100 100" stroke-width="0"></path>
    </svg>
  </div>
  
  
  
  <!-- our_partners -->
  <section class="our_partners">
    <div class="wrapper">
    
      <div class="section_title">
        <h2>НАШИ ПАРТНЕРЫ<br> ВЕБ-СТУДИИ</h2>
      </div>
      <span class="decor_text_underline"></span>
      
      <?php get_template_part( 'content/partners' ); ?>
      <a href="rewievs_web_studio/" class="btn_style_2">все отзывы</a>

    </div>
  </section>
  <!-- end our_partners -->

  <div class="decor_line decor_line_top">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
    </svg>
  </div>

  
  <!-- partners_page  -->
  <section class="partners_page partners_page_b2">
    <div class="wrapper">
      
      <div class="section_title">
        <h2>Теперь мы готовы к дальнейшему росту вместе с вами! Посмотрите наши партнерские цены для вас и оставьте заявку для начала сотрудничества!</h2>
      </div>
      <span class="decor_text_underline decor_text_underline-blue"></span>
      
      <br>
      <br>
      <br>
      
      <table>
      <thead>
        <tr>
        <td colspan="3" class="head ">Партнерские цены</td>
        </tr>
      </thead>
      <tbody>
        <tr>
        <td>Месячный объем</td>
        <td>Копирайтинг ($)</td>
        <td>Рерайтинг ($)</td>
        </tr>
        <tr>
        <td>
        &lt;200 тыс.</td>
        <td>2</td>
        <td>1,5</td>
        </tr>
        <tr>
        <td>&gt;200 тыс.</td>
        <td>1,5</td>
        <td>1</td>
        </tr>
      </tbody>
      </table>
     
    </div>
  </section>
  <!-- end partners_page -->


  <div class="decor_line decor_line_bottom">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 100 L100 0 L100 100" stroke-width="0"></path>
    </svg>
  </div>
  
  
  
  
  <!-- consultation -->
  <section class="consultation">
    <div class="wrapper">
      
      <div class="section_title">
        <h2>Напишите нам</h2>
      </div>
      <span class="decor_text_underline"></span>
      
      
      <div class="consultation__form__wrap">
        <form class="form_send_text" >
           <div class="form_row">
            <input type="text" name="Имя" class="input_text" required placeholder="Ваше имя*">
           </div>
           <div class="form_row">
            <input type="tel" name="Телефон" class="input_text user_phone_mask" required>
           </div>
           <div class="form_row">
            <input type="email" name="E-mail" class="input_text" required placeholder="Ваш e-mail*">
           </div>
           <div class="form_row">
            <input type="text" name="Название компании" class="input_text" placeholder="Название компании или Ваш сайт">
           </div>
           <div class="form_row">
             <textarea class="textarea_form" name="Комментарий" placeholder="Что вас интересует? Опишите кратко: для какого проекта, что именно хотите заказать, какие объемы планируются, проект разовый или на постоянной основе?"></textarea>
           </div>

          <!-- hidden input -->
          <input type="hidden" name="form_subject"  value="Заявка">
          <input type="hidden" name="form_title"    value="Со страницы партнеров">
          <input type="hidden" name="form_redirect" value="">
          <input type="hidden" name="btn_response"  value="Отправлено">
          <input type="hidden" name="action"        value="form_send_text">
          <!-- hidden input -->

           <div class="form_row">
            <input type="submit" class="input_submit" value="Отправить">
           </div>
        </form>
      </div>

    </div>
  </section>
  <!-- end consultation -->
  
  
  <div class="decor_line decor_line_top">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
    </svg>
  </div>
  
  
  <!-- partners_page_b3 -->
  <section class="our_clients partners_page_b3">
    <div class="wrapper">
      
      <div class="section_title">
        <h2>НАШИ ПАРТНЕРЫ ВЕБ-СТУДИИ</h2>
      </div>
      <span class="decor_text_underline decor_text_underline-blue"></span>

      <div class="our_clients__wrap">
        <div class="grid_wrap">

          <?php
            global $post;
            $args = array( 'post_type' => 'partners_blog', 'order' => 'ASC', 'posts_per_page' => -1 );
            $name = get_posts( $args );
            foreach( $name as $post ){ setup_postdata($post);
          ?>

          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="<?php echo get_post_meta( $post->ID, 'site_url', true ); ?>" target="_blank" rel="nofollow" >
                <?php the_post_thumbnail(); ?>
              </a>
              <span class="our_clients__item__descrip"><span><?php the_title(); ?></span></span>
            </div>
          </div>
            
          <?php
            }
            wp_reset_postdata();
          ?>
         
        </div>
      </div>
      
    </div>
  </section>
  <!-- end partners_page_b3 -->


<?php get_footer(); ?>