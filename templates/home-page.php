<?php
  /*
  Template Name: Главная
  */
  get_header();
?>




  <!-- main_screen -->
  <section class="main_screen">
    <div class="wrapper">
      <div class="main_screen__wrap">
        <div class="main_screen__slogan" >
          <h1>Агентство<br>копирайтинга</h1>
        </div>
        <div class="section_subtitle" id="write_text" data-text="Грамотно. Профессионально. Не банально">
        </div>
        <span class="decor_text_underline"></span>
      </div>
    </div>
    
    <div class="decor_line decor_line_top">
      <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
        <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
      </svg>
    </div>
  </section>
  <!-- end main_screen -->
  
  
  <!-- our_services -->
  <section class="our_services">
    <div class="wrapper">
      <div class="section_title">
        <h2>НАШИ УСЛУГИ</h2>
      </div>
      <div class="section_subtitle">
        <p>Закажите текстовый контент для продвижения вашего сайта</p>
      </div>
      <span class="decor_text_underline decor_text_underline-blue"></span>
      
      <div class="our_services__wrap">
        <div class="grid_wrap">
         
          <div class="grid_coll_25">
            <div class="our_services__item">
              <div class="our_services__img">
                <a href="seo-kopirighting-rewriting/">
                  <img src="<?= REL_ASSETS_URI; ?>img/home/services_1.png" alt="">
                </a>
              </div>
              <a href="seo-kopirighting-rewriting/" class="title">SEO-копирайтинг</a>
              <p class="text">Повышайте трафик, оптимизируя статьи под поисковые системы.</p>
            </div>
          </div>
         
          <div class="grid_coll_25">
            <div class="our_services__item">
              <div class="our_services__img">
                <a href="sales_texts/">
                  <img src="<?= REL_ASSETS_URI; ?>img/home/services_2.png" alt="">
                </a>
              </div>
              <a href="sales_texts/" class="title">Продающие тексты</a>
              <p class="text">Помогите клиентам принять правильное решение при выборе ваших товаров и услуг.</p>
            </div>
          </div>
         
          <div class="grid_coll_25">
            <div class="our_services__item">
              <div class="our_services__img">
                <a href="text_audit/">
                  <img src="<?= REL_ASSETS_URI; ?>img/home/services_3.png" alt="">
                </a>
              </div>
              <a href="text_audit/" class="title">Текстовый аудит</a>
              <p class="text">Мы просканируем все тексты на вашем сайте, чтобы сделать их более конверсионными!</p>
            </div>
          </div>
         
          <div class="grid_coll_25">
            <div class="our_services__item">
              <div class="our_services__img">
                <a href="text_redaction/">
                  <img src="<?= REL_ASSETS_URI; ?>img/home/services_4.png" alt="">
                </a>
              </div>
              <a href="text_redaction/" class="title">Редактура текстов</a>
              <p class="text">Скажите «ДА» грамотному тексту - закажите проверку у наших редакторов!</p>
            </div>
          </div>
          
        </div>
      </div>
      
    </div>
  </section>
  <!-- end our_services -->
  
  <div class="decor_line decor_line_bottom">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 100 L100 0 L100 100" stroke-width="0"></path>
    </svg>
  </div>
  
  <!-- about_us -->
  <section class="about_us">
    <div class="wrapper">
      <div class="section_title">
        <h2>о нас</h2>
      </div>
      <span class="decor_text_underline"></span>
      
      <div class="about_us__wrap">
        <p>Почему «Руки с Ручками» («Hands with Pens» с англ. «Руки с Ручками»)? Да потому что мы родились уже с ними в руках, сразу определив свою судьбу и род занятий в жизни!</p>
        <p>Люди после наших текстов выходят покурить, а поисковики пропускают их без очереди. Получив заказ, мы загораемся, а остываем только после сдачи проекта. </p>
        <p>Хотите с нами? Пойдемте, и мы покажем вам мир высокого трафика!</p>
      </div>

    </div>
  </section>
  <!-- end about_us -->
  
  <div class="decor_line decor_line_top">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
    </svg>
  </div>
  
  
  <!-- reviews -->
  <section class="reviews">
    <div class="wrapper">
      <div class="section_title">
        <h2>отзывы</h2>
      </div>
      <span class="decor_text_underline decor_text_underline-blue"></span>
      
      <div class="reviews__wrap">
        <div class="reviews__slider">

          <?php
            global $post;
            $args = array( 'post_type' => 'reviews', 'order' => 'ASC', 'posts_per_page' => -1 );
            $name = get_posts( $args );
            foreach( $name as $post ){ setup_postdata($post);
          ?>

          <div class="reviews__item">
            <div class="reviews__item__img">
              <?php the_post_thumbnail(); ?>
            </div>
            <div class="reviews__item__text">
              <?php the_content(); ?>
            </div>
          </div>
            
          <?php
            }
            wp_reset_postdata();
          ?>
         
        </div>
        
        <span class="slider_prev"></span>
        <span class="slider_next"></span>
      </div>
      
      <a href="rewievs/" class="btn_style_1">все отзывы</a>

    </div>
  </section>
  <!-- end reviews -->
  
  
  <div class="decor_line decor_line_bottom">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 100 L100 0 L100 100" stroke-width="0"></path>
    </svg>
  </div>
  
  
  <!-- our_partners -->
  <section class="our_partners">
    <div class="wrapper">
    
      <div class="section_title">
        <h2>НАШИ ПАРТНЕРЫ<br> ВЕБ-СТУДИИ</h2>
      </div>
      <span class="decor_text_underline"></span>
      
      <?php get_template_part( 'content/partners' ); ?>
      <a href="rewievs_web_studio/" class="btn_style_2">все отзывы</a>

    </div>
  </section>
  <!-- end our_partners -->
  
  <div class="decor_line decor_line_both_top">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 100 L50 0 L100 100" stroke-width="0"></path>
    </svg>
  </div>
  
  <!-- our_clients -->
  <section class="our_clients">
    <div class="wrapper">
      
      <div class="section_title">
        <h2>НАШИ КЛИЕНТЫ</h2>
      </div>
      <span class="decor_text_underline decor_text_underline-blue"></span>

      <div class="our_clients__wrap">
        <div class="grid_wrap">
         
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="http://greenyshop.com.ua/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/greenyshop.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>Greeny Shop</span></span>
            </div>
          </div>
         
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="http://www.terradeck.ru/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/terradeck.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>Terradeck</span></span>
            </div>
          </div>
         
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="http://sytki-minsk.com/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/sutkiminsk.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>Sutki-Minsk</span></span>
            </div>
          </div>
         
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="http://absolutist.ru/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/absolutist.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>Absolutist</span></span>
            </div>
          </div>
          
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="https://semantica.in/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/semantica.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>SEMANTICA</span></span>
            </div>
          </div>
          
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="https://www.toypiter.ru/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/toypiter.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>ToyPiter</span></span>
            </div>
          </div>
          
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="http://www.toursjapan.ru/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/toursjapan.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>ToursJapan</span></span>
            </div>
          </div>
          
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="https://akym.com.ua/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/akymcomua.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>Akym.com.ua</span></span>
            </div>
          </div>
          
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="http://www.oknaekb66.ru/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/evrodesign.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>ЕвроДизайн</span></span>
            </div>
          </div>
          
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="http://nsmarine.ru/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/newstarmarine.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>NewStar Marine</span></span>
            </div>
          </div>
          
        </div>
      </div>
      
    </div>
  </section>
  <!-- end our_clients -->


<?php get_footer(); ?>