<?php
  /*
  Template Name: Услуги
  */
  get_header();
?>



  <!-- top_screen -->
  <section class="top_screen">
    <div class="wrapper">
      <div class="section_title">
        <h1>Наши услуги</h1>
      </div>
      <div class="section_subtitle">
        <p>Какие услуги Hands with Pens нужны вам в продвижении сайта<br>и создании крутого контента?</p>
      </div>
      <span class="decor_text_underline"></span>
    </div>
  </section>
  <div class="decor_line decor_line_top">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
    </svg>
  </div>
  <!-- end top_screen -->
  
  
  <!-- services_page_b1 -->
  <section class="services_page services_page_b1">
    <div class="wrapper">
      <div class="services_page_title">
        <h2>SEO-копирайтинг и<br>рерайтинг</h2>
      </div>
      
      <p>SEO-текст – ваш боевой товарищ №1. Оптимизация статей ключами нужна для того, чтобы вы получали бесплатный органический трафик из поисковых систем.</p>
      
      <p>Вам важно заказать SEO-копирайтинг для наполнения сайта текстами с ключами (главные страницы сайта и статьи в блог), заполнения категорий товаров и услуг, карточек товаров вашего интернет-магазина. Продвижение упростит написание информационно-полезных статей, всегда актуальна публикация новостей и релизов.</p>
      
      <p>SEO-копирайтинг и рерайтинг имеющихся статей поднимут ваш сайт в ТОП поисковой выдачи, как результат – увеличат трафик, а это + к росту продаж. Они же положительно влияют на поведенческие факторы – качественный контент повышает доверие к вашему ресурсу. На вас будут ссылаться, и вас станут выбирать!</p>
      
      <span class="decor_text_underline decor_text_underline-blue"></span>
      
      <a href="price/" class="services_page_know_price btn_style_1" >Узнать цены</a>
    
    </div>
  </section>
  <!-- end services_page_b1 -->
  
  <div class="decor_line decor_line_bottom">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 100 L100 0 L100 100" stroke-width="0"></path>
    </svg>
  </div>
  
  <!-- services_page_b2 -->
  <section class="services_page services_page_b2">
    <div class="wrapper">
    
      <div class="services_page_title">
        <h2>Продающие тексты</h2>
      </div>
      
      <p>Сайт – ваш менеджер по продажам, который работает в режиме нон-стоп 24/7. А чтобы он действительно продавал и вы получали прибыль, важно заполнить страницы «специальным» контентом. Продающие тексты превратят простого гостя в покупателя, а покупателя в постоянного клиента! Что это значит для вас? Правильно – успех вашего дела и бизнеса в целом!</p>
     
      <span class="decor_text_underline"></span>
      
      <a href="price/" class="services_page_know_price btn_style_2" >Узнать цены</a>

      <div class="services_page_decor">
        <svg height="120px" preserveAspectRatio="none"  version="1.1" viewBox="0 0 100 100" width="240px" xmlns="http://www.w3.org/2000/svg">
          <path d="M0 100 L50 0 L100 100" stroke-width="0"></path>
        </svg>
      </div>
      
    </div>
  </section>
  <!-- end services_page_b2 -->
  
  
  <!-- services_page_b3 -->
  <section class="services_page services_page_b3">
    <div class="wrapper">
    
      <div class="services_page_title">
        <h2>Текстовый аудит</h2>
      </div>
      
      <p>У вас уже запущен проект, но трафик маловат и посетители не доходят до целевого действия? Взгляните на тексты. Объективно оценить свой ресурс сложно, а потому важно обратиться к команде, которая с текстами на «ты». Возможно, где-то когда-то вы свернули не туда? Убедитесь, что с контентом нет проблем! И если необходимо, мы оперативно подкорректируем места, где есть «пробоины».</p>
      
      <p>Предоставим рекомендации по улучшению текстов на главных страницах с точки зрения презентации, дадим комментарии к продающим текстам для улучшения их конверсионности, пропишем список пунктов по улучшению SEO-текстов к описаниям товаров и услуг, а также текстов в блоге. Сделаем рерайтинг любых 2-х текстов на сайте до 3000 символов, на которые вы укажите самостоятельно.</p>
      
      <span class="decor_text_underline decor_text_underline-blue"></span>
      
      <a href="price/" class="services_page_know_price btn_style_1" >Узнать цены</a>

    </div>
  </section>
  <!-- end services_page_b3 -->
  
  
  <!-- services_page_b4 -->
  <section class="services_page services_page_b4">
    <div class="wrapper">
    
      <div class="services_page_decor">
        <svg  height="120px" preserveAspectRatio="none"  version="1.1" viewBox="0 0 100 100" width="120px" xmlns="http://www.w3.org/2000/svg">
          <circle cx="50" cy="50" r="50" stroke-width="0"></circle>
        </svg>
      </div>
    
      <div class="services_page_title">
        <h2>Редактура текстов</h2>
      </div>
      
      <p>«Чистые» тексты – ваше все. Сложно доверять компании, заказывать у нее продукт или услугу, если доставка "безплатная", а товар вам гарантированно "понравиться". Даже при условии, что все продукты высокого качества. Не теряйте клиентов из-за таких оплошностей! Мы исправим орфографические и пунктуационные ошибки, поработаем над логикой и стилистикой. Редактирование и корректировка сделает тексты совершенными, прибавит вам очков!</p>
      
      <p>Корректура представляет собой исправление орфографических, пунктуационных и стилистических ошибок в словах и фразах. Редактура  - это переписывание частей текста для того, чтобы сделать его более продающим, конверсионным или полезным (исходя из потребности). Услуга включает в себя и корректуру ошибок.</p>
      
      <span class="decor_text_underline"></span>
      
      <a href="price/" class="services_page_know_price btn_style_2" >Узнать цены</a>

    </div>
  </section>
  <!-- end services_page_b4 -->

  <div class="decor_line decor_line_top">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
    </svg>
  </div>
  

<?php get_footer(); ?>