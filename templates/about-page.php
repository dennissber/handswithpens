<?php
  /*
  Template Name: О нас
  */
  get_header();
?>




  <!-- top_screen -->
  <section class="top_screen">
    <div class="wrapper">
      <div class="section_title">
        <h1>О нас</h1>
      </div>
      <span class="decor_text_underline"></span>
    </div>
  </section>
  <!-- end top_screen -->
  
  
  <!-- about_page -->
  <section class="about_page">
    <div class="wrapper">
      <p>Мы – копирайтеры от кончиков пальцев до последнего нейрона головного мозга. Каждый текст для нас – детище, выношенное и выпестованное с любовью до последней буквы. При этом вам не придется ждать 9 месяцев, чтобы увидеть его! Благодаря техническому прогрессу ручка успешно заменилась для нас клавиатурой. Так что процесс пошел быстрее, спасибо Шоулзу, Паскалю и всем крутым ребятам, которые придумали пишущие машинки и компьютеры.</p>
      
      <p>Но нотка консерватизма в нас таки осталась. К примеру, мы неприемлем ошибки. И плевать, что сейчас грамотность в упадке! Мы – за традиции, а значит – за неукоснительное и непреклонное следование стандартам литературной речи. А это, напомним-с, плюсик для вас! Забудьте про наем дополнительного редактора, к чему такие траты?</p>
      
      <p>Тексты от Hands with Pens – это грамотно, профессионально, не банально. Сделайте свой сайт еще привлекательнее и эффективнее вместе с нами!</p>
      
      <br>
      <br>
      <br>
      
      <h2>Почему стоит работать с нами?</h2>
      <p>Для кого-то копирайтинг – адский труд, для нас – любимое дело. А потому тексты получаются легкими и приятными. Мы знаем свою сферу от начала до конца, каждое слово в предложении проходит контроль качества. Наше кредо – только полезные тексты, только актуальная информация!</p>
      <span class="decor_text_underline"></span>
      
      <br>
      <br>
      
      <div class="about_page__question">
        <div class="about_page__question__row title">
          1. КАК Я МОГУ ВАМ ДОВЕРЯТЬ? Я ЖЕ НЕ ЗНАЮ, КАК ВЫ ПИШЕТЕ!
        </div>
        <div class="about_page__question__row text">
          Первый текст для вас – бесплатный, чтобы вы сразу оценили качество нашей работы, выбрали подходящий стиль и убедились, что нам можно доверять свой проект.
        </div>
        <div class="about_page__question__row title">
          2. СКОЛЬКО ТЕКСТОВ МОЖНО ЗАКАЗАТЬ?
        </div>
        <div class="about_page__question__row text">
          Написать 10 текстов или весь сайт на 100500 разделов – ваш проект любого масштаба будет готов в срок, без задержек и опозданий.
        </div>
        <div class="about_page__question__row title">
          3. НУЖЕН МИЛЛИОН СТАТЕЙ, НО БЮДЖЕТ НЕ РЕЗИНОВЫЙ. ЧТО ДЕЛАТЬ?
        </div>
        <div class="about_page__question__row text">
          С нами все реально, ведь чем крупнее ваш заказ, тем дешевле обойдется его выполнение! Стоимость заказа текстов вы можете увидеть в разделе «Цены».
        </div>
        <div class="about_page__question__row title">
          4. А ЧТО НАСЧЕТ СТИЛЯ?
        </div>
        <div class="about_page__question__row text">
          Дикий креатив или сдержанный деловой стиль, разговорный или официальный – наши авторы подберут для вашего сайта наиболее уместный слог!
        </div>
        <div class="about_page__question__row title">
          5. КАК МЫ БУДЕМ РАБОТАТЬ?
        </div>
        <div class="about_page__question__row text">
          Никаких 150 инстанций и согласований, ведь ваш личный редактор будет вести проект от обсуждения заказа и до получения вашего одобрения.
        </div>
      </div>
      
      
    </div>
  </section>
  <!-- end about_page -->

  <div class="decor_line decor_line_top">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
    </svg>
  </div>


<?php get_footer(); ?>