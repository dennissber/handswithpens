<?php
  /*
  Template Name: Цены
  */
  get_header();
?>



  <!-- top_screen -->
  <section class="top_screen">
    <div class="wrapper">
      <div class="section_title">
        <h1>Цены</h1>
      </div>
      <span class="decor_text_underline"></span>
    </div>
  </section>
  <!-- end top_screen -->
  
  
  <!-- price_page_b1 -->
  <section class="price_page price_page_b1">
    <div class="wrapper">
     
       <p>Тексты на русском языке - стандартные и партнерские цены для WEB-студий и SEO-специалистов</p>
       
       <div class="price_page__prices">
         <div class="grid_wrap">
           <div class="grid_coll_33">
            <div class="price_page__item">
              <table>
                <thead>
                  <tr>
                  <td colspan="3" class="head">Для WEB-студий и SEO-специалистов </td>
                  </tr>
                </thead>
              
                <tbody>
                  <tr>
                    <td>Месячный объем (кол-во симв. с пробелами)</td>
                    <td>Копирайтинг ($ за 1000 симв. с пробелами)</td>
                    <td>Рерайтинг ($ за 1000 симв. с пробелами)</td>
                  </tr>
                  <tr>
                    <td>&lt;100 тыс.</td>
                    <td>2,5</td>
                    <td>2</td> 
                  </tr>
                  <tr>
                    <td>&gt;100 тыс.</td>
                    <td>2</td>
                    <td>1,5</td>
                  </tr>
                </tbody>
              </table>
            </div>
           </div>
           
           <div class="grid_coll_33">
            <div class="price_page__item">
              <table>
                <thead>
                  <tr>
                    <td colspan="3" class="head">Партнерские цены для WEB-студий</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Месячный объем</td>
                    <td>Копирайтинг ($)</td>
                    <td>Рерайтинг ($)</td>
                  </tr>
                  <tr>
                    <td>&lt;200 тыс.</td>
                    <td>2</td>
                    <td>1,5</td>
                  </tr>
                  <tr>
                    <td>&gt;200 тыс.</td>
                    <td>1,5</td>
                    <td>1</td>
                  </tr>
                </tbody>
              </table>
            </div>
           </div>
           
           <div class="grid_coll_33">
            <div class="price_page__item">
              <table>
                <thead>
                  <tr>
                    <td colspan="3" class="head">Дополнительные услуги</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Корректура (за 1000 симв. с пробелами)</td>
                    <td>Редактура (за 1000 симв. с пробелами)</td>
                    <td>Текстовый аудит сайта</td>
                  </tr>
                  <tr>
                    <td>0,7$</td>
                    <td>1,2$</td>
                    <td>20$</td>
                  </tr>
                </tbody>
              </table>
            </div>
           </div>
           
         </div>
       </div>
       
      <p>Тексты на английском языке - стандартные и партнерские цены для WEB-студий и SEO-специалистов</p>
      
      <div class="price_page__prices_2">
        <div class="grid_wrap">
          <div class="grid_coll_50">
            <div class="price_page__item">
              <table>
              <thead>
                <tr>
                  <td colspan="3" class="head">Стандартные цены</td>
                </tr>
              </thead>
              
              <tbody>
                <tr>
                <td>ENGLISH</td>
                <td>Копирайтинг $</td>
                <td>Рерайтинг $</td>
                </tr>
                <tr>
                <td>&lt;50 тыс.</td>
                <td>5</td>
                <td>4</td>
                </tr>
                <tr>
                <td>&gt;50 тыс.</td>
                <td>4</td>
                <td>3</td>
                </tr>
              </tbody>
              </table>
            </div>
            
          </div>
          <div class="grid_coll_50">
            <div class="price_page__item">
              <table>
              <thead>
                <tr>
                  <td colspan="3" class="head">Для WEB-студий</td>
                </tr>
              </thead>
              
              <tbody>
                <tr>
                <td>ENGLISH</td>
                <td>Копирайтинг $</td>
                <td>Рерайтинг $</td>
                </tr>
                <tr>
                <td>&lt;50 тыс.</td>
                <td>4</td>
                <td>3</td>
                </tr>
                <tr>
                <td>&gt;50 тыс.</td>
                <td>3</td>
                <td>2</td>
                </tr>
              </tbody>
              </table>
            </div>
            
          </div>
        </div>
      </div>
      
      
      
      <div class="price_page_order_btn__wrap">
        <a href="#order" class="price_page_order_btn btn_style_2 scrollhook" >Заказать</a>
      </div>
      
      
      <div class="price_page__list_wy">
      
        
        <div class="services_page_title">
          <h2>Почему оно того стоит?</h2>
        </div>  
        
        <span class="decor_text_underline"></span>
      
        <ul class="price_page__list">
           
            <li class="grid_wrap">
              <div class="grid_coll_30">
                <div class="price_page__list__ico">
                  <i class="fa fa-wrench"></i>
                </div>
              </div>
              <div class="grid_coll_70">
                <div class="price_page__list__text">
                  <h2 class="price_page__list__title">
                    Одно вложение - постоянный результат
                  </h2>
                  <p>
                    Тексты будут работать на вас 24 часа и 7 дней в неделю – согласитесь, это выгодное вложение. Вы ничего не делаете, просто наблюдаете за ростом трафика и прибыли!          
                  </p>
                </div>
              </div>
            </li>
            
            <li class="grid_wrap">
              <div class="grid_coll_30">
                <div class="price_page__list__ico">
                  <i class="fa fa-rocket"></i>
                </div>
              </div>
              <div class="grid_coll_70">
                <div class="price_page__list__text">
                  <h2 class="price_page__list__title">
                    Оперативность - наше все
                  </h2>
                  <p>
                    Заказывая копирайтинг у нас, вы можете быть уверены не только в качестве текстового контента, но и в быстроте его выполнения. Мы ценим ваше время!           
                  </p>
                </div>
              </div>
            </li>
            
            <li class="grid_wrap">
              <div class="grid_coll_30">
                <div class="price_page__list__ico">
                  <i class="fa fa-star"></i>
                </div>
              </div>
              <div class="grid_coll_70">
                <div class="price_page__list__text">
                  <h2 class="price_page__list__title">
                    Все по высшему разряду!
                  </h2>
                  <p>
                    Наша политика: оптимальное соотношение всех параметров работы. Звездочка за скорость, звездочка за прайс, звездочка за интересность, звездочка за актуальность, звездочка за оптимизацию. Убедитесь сами, что тексты на сайт от Hands with Pens пятизвездочные!        
                  </p>
                </div>
              </div>
            </li>
            
            <li class="grid_wrap">
              <div class="grid_coll_30">
                <div class="price_page__list__ico">
                  <i class="fa fa-android"></i>
                </div>
              </div>
              <div class="grid_coll_70">
                <div class="price_page__list__text">
                  <h2 class="price_page__list__title">
                    Актуальность
                  </h2>
                  <p class="text-left">
                    Google - буквально король Сети на сегодняшний день. Так идите в ногу с королем! Как разработка от Google - Android - занимает современный рынок смартов на 82%, так и SEO-копирайтинг, оптимизация под Google - это больше 80% успеха в продвижении вашего вебсайта.            
                  </p>
                </div>
              </div>
            </li>
          </ul>
        </div>
    
    </div>
  </section>
  <!-- end price_page_b1 -->
  
  <div class="decor_line decor_line_both_top">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 100 L50 0 L100 100" stroke-width="0"></path>
    </svg>
  </div>
  
  
  <!-- our_clients -->
  <section class="our_clients">
    <div class="wrapper">
      
      <div class="section_title">
        <h2>НАШИ КЛИЕНТЫ</h2>
      </div>
      <span class="decor_text_underline decor_text_underline-blue"></span>

      <div class="our_clients__wrap">
        <div class="grid_wrap">
         
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="http://greenyshop.com.ua/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/greenyshop.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>Greeny Shop</span></span>
            </div>
          </div>
         
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="http://www.terradeck.ru/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/terradeck.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>Terradeck</span></span>
            </div>
          </div>
         
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="http://sytki-minsk.com/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/sutkiminsk.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>Sutki-Minsk</span></span>
            </div>
          </div>
         
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="http://absolutist.ru/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/absolutist.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>Absolutist</span></span>
            </div>
          </div>
          
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="https://semantica.in/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/semantica.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>SEMANTICA</span></span>
            </div>
          </div>
          
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="https://www.toypiter.ru/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/toypiter.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>ToyPiter</span></span>
            </div>
          </div>
          
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="http://www.toursjapan.ru/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/toursjapan.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>ToursJapan</span></span>
            </div>
          </div>
          
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="https://akym.com.ua/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/akymcomua.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>Akym.com.ua</span></span>
            </div>
          </div>
          
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="http://www.oknaekb66.ru/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/evrodesign.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>ЕвроДизайн</span></span>
            </div>
          </div>
          
          <div class="grid_coll_20">
            <div class="our_clients__item">
              <a href="http://nsmarine.ru/" target="_blank" rel="nofollow" >
                <img src="<?= REL_ASSETS_URI; ?>img/home/newstarmarine.png" alt="">
              </a>
              <span class="our_clients__item__descrip"><span>NewStar Marine</span></span>
            </div>
          </div>
          
        </div>
      </div>
      
    </div>
  </section>
  <!-- end our_clients -->
  
  
  <div class="decor_line decor_line_bottom">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 100 L100 0 L100 100" stroke-width="0"></path>
    </svg>
  </div>
  
  <!-- consultation -->
  <section class="consultation" id="order">
    <div class="wrapper">
      
      <div class="section_title">
        <h2>Оставьте заявку на<br>консультацию или заказ</h2>
      </div>
      <span class="decor_text_underline"></span>
      
      
      <div class="consultation__form__wrap">
        <form class="form_send_text" >
           <div class="form_row">
            <input type="text" name="Имя" class="input_text" required placeholder="Ваше имя*">
           </div>
           <div class="form_row">
            <input type="tel" name="Телефон" class="input_text user_phone_mask" required>
           </div>
           <div class="form_row">
            <input type="email" name="E-mail" class="input_text" required placeholder="Ваш e-mail*">
           </div>
           <div class="form_row">
            <input type="text" name="Название компании" class="input_text" placeholder="Название компании или Ваш сайт">
           </div>
           <div class="form_row">
             <textarea class="textarea_form" name="Комментарий" placeholder="Что вас интересует? Опишите кратко: для какого проекта, что именно хотите заказать, какие объемы планируются, проект разовый или на постоянной основе?"></textarea>
           </div>

          <!-- hidden input -->
          <input type="hidden" name="form_subject"  value="Заявка">
          <input type="hidden" name="form_title"    value="Заявка на консультацию">
          <input type="hidden" name="form_redirect" value="">
          <input type="hidden" name="btn_response"  value="Отправлено">
          <input type="hidden" name="action"        value="form_send_text">
          <!-- hidden input -->

           <div class="form_row">
            <input type="submit" class="input_submit" value="Отправить">
           </div>
        </form>
      </div>

    </div>
  </section>
  <!-- end consultation -->
  

  <div class="decor_line decor_line_top">
    <svg preserveAspectRatio="none" version="1.1" viewBox="0 0 100 100"  xmlns="http://www.w3.org/2000/svg">
      <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
    </svg>
  </div>


<?php get_footer(); ?>