$(function (){ 
  
  // other
  float_block( 'header', 'float', true, 1000 );
  top_navigate_mobile();
  
 
  /* float_block 2.0 */
  function float_block( block, add_class, false_block ){
    // param
    var this_b  = $(block);
    var block_t = $(this_b).offset().top;
    var block_h = this_b.outerHeight();
    // if need false block
    if ( false_block ){
      var block_n = "float_block_" + block;
      var false_b = "<div class='" + block_n + "' style='height:" + block_h + "px;' ></div>";
      this_b.wrap(false_b);
    }
    // in load page
    var doc_r  = $(window).scrollTop();
    if ( doc_r + block_h > block_h ){
      this_b.addClass(add_class);
    }
    // in scroll
    $(window).on('scroll', function() {
      var doc_r  = $(window).scrollTop();
      if ( doc_r + block_h > block_h ){
        this_b.addClass(add_class);
      } else {
        this_b.removeClass(add_class);
      }
    });
  }
  
  
  // if Typed
  if ( $("#write_text").length ) {
    var text = $("#write_text").data("text");
    // home main title
    new Typed('#write_text', {
      strings: [text],
      typeSpeed: 40,
      fadeOut: true,
      loop: false,
      cursorChar: '',
    });
  };
  
  
  // if slick 
  if (typeof $(this).slick == 'function') {

    // reviews__slider
    var reviews__slider = $('.reviews__slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      dots: true,
      arrows: false,
      adaptiveHeight: true,
      autoplaySpeed: 5000,
    });
    
    $(".reviews__slider").siblings(".slider_prev").click(function() {
      reviews__slider.slick("slickPrev");
    });
    $(".reviews__slider").siblings(".slider_next").click(function() {
      reviews__slider.slick("slickNext");
    });

    // our_partners__slider
    var our_partners__slider = $('.our_partners__slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      dots: true,
      arrows: false,
      adaptiveHeight: true,
      autoplaySpeed: 5000,
    });
    
    $(".our_partners__slider").siblings(".slider_prev").click(function() {
      our_partners__slider.slick("slickPrev");
    });
    $(".our_partners__slider").siblings(".slider_next").click(function() {
      our_partners__slider.slick("slickNext");
    });

  }
  // END if slick
  
  
  // top_navigate_mobile 2.0
  function top_navigate_mobile(){
    var main_wrap       = $('#main_wrapper');
    var header__nav_btn = $('#header__nav_btn');
    var header__nav     = $('#header__nav_wrap');

    // header__nav for mobile
    $("body").on( "click", '#header__nav_btn', function(){
      if ( !header__nav.data('open') ){
        menu_open();
      } else {
        menu_close();
      }
    });

    function menu_open(){
      header__nav.addClass("open_nav");
      header__nav_btn.addClass("open_nav");
      main_wrap.addClass("open_nav");
      header__nav.data('open', true);

      $("body").on( "mousedown", "#main_wrapper", function(){
        menu_close();
      });
      $("body").on( "touchmove", "#main_wrapper", function(){
        menu_close();
      });
      $("body").on( "touchstart", "#main_wrapper", function(){
        menu_close();
      });

    }
    function menu_close(){
      header__nav.removeClass("open_nav");
      header__nav_btn.removeClass("open_nav");
      main_wrap.removeClass("open_nav");
      header__nav.data('open', false);
    }
  }
  
  

  /* modals */
  function modalOpen(modal){
    $('.'+modal).slideDown();
    overlayOpen();
  }
  function modalClose(){
    $(".modalWindowWrap").slideUp();
    overlayClose();
  }
  $("body").on( "click", "[data-modal]", function(){
    var modal = $(this).attr("data-modal");
    modalOpen(modal);
  });
  $("body").on( "click", function(event){
    var target = $(event.target);
    if ( target.hasClass('modalCell') || target.hasClass('modalWindowClose') ){
      modalClose();
    }
  });


  /* overlay */
  function overlayOpen(){
    $(".modalOverlay").fadeIn();
      $("html, body").addClass('no-scroll');
    }
  function overlayClose(){
    $(".modalOverlay").fadeOut();
    $("html, body").removeClass('no-scroll');
  }
  

  
  // scrollToTop 2.0
  $.fn.scrollToTop = function() {
    if ($(window).scrollTop() != "0") {
      $(this).addClass("show");
    }
    var scrollDiv = $(this);
    $(window).scroll(function() {
      if ($(window).scrollTop() == "0") {
        $(scrollDiv).removeClass("show");
      } else {
        $(scrollDiv).addClass("show");
      }
    });
    $(this).click(function() {
      $("html, body").animate({
        scrollTop: 0
      }, "fast");
    });
  };
  $("#goTop").scrollToTop();
  


   /* masked input */ 
   //var mask = "+7 (999) 999-99-99";
   //var placeholder = {'placeholder':'+7 (___) ___ __ __'};
   var mask = "+38(099) 999-99-99";
   var placeholder = {'placeholder':'+38(0__) ___ __ __'};
   var user_phone = $('.user_phone_mask');

   user_phone.each(function(){
    $(this).mask(mask);
   });
   user_phone.attr(placeholder);


  // scrollhook
  $(".scrollhook").click(function(event){
      event.preventDefault();
      var full_url = this.href;
      var parts = full_url.split("#");
      var trgt = parts[1];
      var target_offset = $("#"+trgt).offset();
      var target_top = target_offset.top - 60;
      $('html, body').animate({scrollTop:target_top}, 1200);
  });


});